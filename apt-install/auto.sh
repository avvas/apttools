#!/usr/bin/env bash
set -eu -o pipefail
[ $# -eq 0 ] && exit 1

# Install selected packages and mark as automatically installed.
# https://askubuntu.com/a/962942

pkgs=$(comm -23 \
	<(xargs -n1 <<< "$@" | sort) \
	<(apt-mark showmanual | sort) |
	xargs)

if [ $(wc -w <<< $pkgs) -gt 0 ]; then
	sudo -- sh -c "apt install $pkgs; apt-mark auto $pkgs"
fi
