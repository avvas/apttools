#!/usr/bin/env bash
set -eu -o pipefail

# Bulk install packages, without prompts and recommends.

pkgs=($(cat | sed '/^#/d' | xargs))
[ ${#pkgs[@]} -eq 0 ] && exit 1

args=(
	--no-install-recommends
	--no-install-suggests
)

opts=(
	-o Dpkg::Options::="--force-confdef"
	-o Dpkg::Options::="--force-confold"
)

# --force-confold retains the old version of the file
# --force-confdef option tells dpkg to decide by itself when possible
# https://debian-handbook.info/browse/stable/sect.package-meta-information.html#sect.conffiles

# dry-check
apt ${opts[@]} install ${args[@]} --simulate ${pkgs[@]} | pager
read; sudo apt ${opts[@]} install ${args[@]} ${pkgs[@]}
