#!/usr/bin/env bash
set -eu -o pipefail
[ $# -ne 1 -o ! -f "${1:-}" ] && exit 1

echo
source "$1"; repo_name=$(basename -- "$1" .sh)
source "$(dirname -- "$0")/config/paths.sh"

echo "- fetching keyring"

wget -qO- "$apt_keyring_url" | sudo gpg --dearmor -o $apt_keyring_file

# | sudo install -D -m 644 /dev/stdin "$apt_keyring_file"
# -o root -g root

echo "- adding new entry"

echo "deb [ signed-by=$apt_keyring_file ] \
$apt_sources_url ${apt_dist:-$repo_name} main" |
	sudo tee "$apt_sources_file" > /dev/null
# trusted=yes

echo "- pinning packages"
# sudo mkdir -p /etc/apt/preferences.d/pins/

pin_origin=$(awk -F[/:] '{print $4}' <<< "$apt_sources_url")

cat <<EOF | sudo tee -- "$apt_pinning_file" > /dev/null
Package: *
Pin: origin $pin_origin
Pin-Priority: 1

Package: ${pkgs[@]}
Pin: origin $pin_origin
Pin-Priority: 500
EOF

echo -e "\nrepository is added! to install packages, run:"
echo -e "sudo apt update && sudo apt install ${pkgs[@]}\n"
