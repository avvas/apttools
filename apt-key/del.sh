#!/usr/bin/env bash
set -eu -o pipefail
[ $# -ne 1 ] && exit 1

echo
echo "- removing third-party repository"
source "$1"; repo_name=$(basename -- "$1" .sh)
source "$(dirname -- "$0")/config/paths.sh"

sudo rm -iv -- "$apt_keyring_file" "$apt_sources_file" "$apt_pinning_file" || :;
echo
