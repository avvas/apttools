#!/usr/bin/env bash
set -eu -o pipefail
[ $# -lt 2 ] && exit 1

# Loop over versions and return the closest preceding value
# (e.g.: `set -- ${installed:=1.1} 0.9 1.2` will return 0.9).

installed=$(cut -d: -f2- <<< $1) # cut deb-version's epoch
available=$(printf '%s\n' ${@:2} | grep -v '^-' | sort -Vu)
# for latter nearest value, set "ge" operator and "sort -Vur"

set -- $available

for recent; do
	if $(dpkg --compare-versions $recent le $installed)
	then rounded=$recent; else break; fi
done

echo ${rounded:-$recent}
