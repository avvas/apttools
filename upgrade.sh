#!/usr/bin/env bash
set -e; echo

echo "- synchronizing package index"
sudo -- apt-get update; echo

echo "- purging unneeded dependencies"
sudo -- apt autopurge; echo

echo "- upgrading installed packages"
sudo -- aptitude safe-upgrade; echo

echo "- removing retrieved files"
sudo -- apt autoclean; echo
